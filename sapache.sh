#!/bin/bash

# Démarrage ou arrêt du conteneur Apache
if [ "$1" == "start" ]; then
  docker run -d -p 8082:80 -v /Users/hugobaras/Documents/Cours/intégrationcontinue/site:/usr/local/apache2/htdocs/ --name apache httpd
  echo "Groupe 10: Martin Dié, Hugo Baras"
  echo "Lancement du serveur apache sur le container"
elif [ "$1" == "stop" ]; then
  docker stop apache
  docker rm apache
  echo "Groupe 10: Martin Dié, Hugo Baras"
  echo "Arrêt du container"

else
  echo "Utilisation : sapache.sh [start|stop]"
fi

