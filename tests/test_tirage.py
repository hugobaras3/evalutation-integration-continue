import pytest
import tempfile
import os
from .. import functions

TEST_CSV_PATH = "path/to/test_data.csv"

@pytest.fixture
def test_csv_file():
    with open(TEST_CSV_PATH, 'w', newline='') as file:
        writer = csv.writer(file, delimiter=';')
        writer.writerow(['Nom', 'Prénom'])
        writer.writerow(['Doe', 'John'])
        writer.writerow(['Smith', 'Jane'])

    yield TEST_CSV_PATH

    os.remove(TEST_CSV_PATH)

def test_read_csv(test_csv_file):
    data = read_csv(test_csv_file)
    assert len(data) == 2
    assert data[0] == ['Doe', 'John']
    assert data[1] == ['Smith', 'Jane']

def test_create_groups_even_number_of_students(test_csv_file):
    students_data, groups = create_groups(test_csv_file)
    assert len(students_data) % 2 == 0
    assert len(groups) == len(students_data) // 2

def test_create_groups_odd_number_of_students():
    with pytest.raises(ValueError):
        create_groups("path/to/odd_number_of_students.csv")

def test_write_result_csv(test_csv_file):
    _, groups = create_groups(test_csv_file)
    
    with tempfile.NamedTemporaryFile(mode='w', delete=False, newline='') as temp_file:
        temp_file_path = temp_file.name
        write_result_csv(temp_file_path, groups)

        with open(temp_file_path, 'r') as file:
            lines = file.readlines()
            assert len(lines) == len(groups) + 1  
            for i, line in enumerate(lines[1:]):  
                assert f'Groupe {i + 1}' in line
                assert 'Etudiant 1' in line
                assert 'Etudiant 2' in line

    os.remove(temp_file_path)
