#!/bin/sh

JENKINS_HOME=/Users/hugobaras/Documents/Cours/intégrationcontinue/jenkins/runtime/workspace

if [ "$1" == "start" ]; then
    echo "Groupe 10: Martin Dié, Hugo Baras"
    echo "Lancement de jenkins sur le port 9090/jenkins"
    java -jar ${JENKINS_HOME}/jenkins.war --httpPort=9090 --prefix=/jenkins
elif [ "$1" == "stop" ]; then
    pid=$(ps aux | grep 'jenkins.war' | grep -v grep | awk '{print $2}')
    if [ -n "$pid" ]; then
        kill $pid
        echo "Groupe 10: Martin Dié, Hugo Baras"
        echo "Arrêt de jenkins"
    else
        echo "Groupe 10: Martin Dié, Hugo Baras"
        echo "Aucun processus Jenkins en cours d'exécution."
    fi
else
    echo "Groupe 10: Martin Dié, Hugo Baras"
    echo "Utilisation : sapache.sh [start|stop]"
fi
